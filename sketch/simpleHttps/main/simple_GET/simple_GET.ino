
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>  
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include <FS.h>

#include <NTPClient.h>
#include <WiFiUdp.h>
#include <CertStoreBearSSL.h>
WiFiClientSecure client;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
String datarx; //Received data as string
#include <LittleFS.h>
void setup() {
  Serial.begin(115200);
  WiFi.begin("bydzovsky2.4", "Byd2@5294Zelb!");
  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(++i); Serial.print('.');
  }
  Serial.println("Setup!");
  setClock();

  timeClient.begin();
  timeClient.setUpdateInterval(43200000);
  SPIFFS.begin();
}

void setClock() {
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");  Serial.print("Waiting for NTP time sync: ");
  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2) {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println("");
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print("Current time: ");
  Serial.print(asctime(&timeinfo));
}

void loop3(){
  WiFiClientSecure httpsClient;
  httpsClient.setTimeout(15000);
  httpsClient.setFingerprint("xx");
  delay(1000);
  int retry = 0;
  const char *host = "circusofthings.com";
  const char *path = "/WriteValue";
  while ((!httpsClient.connect(host, 443)) && (retry < 15)) {
    delay(100);
    Serial.print(".");
    retry++;
  }
  if (retry == 15) {
    Serial.println("Connection failed");
  }
  else {
    Serial.println("Connected to Server");
  }
  StaticJsonDocument<300> jsonDoc;
  JsonObject root = jsonDoc.to<JsonObject>();
  root["Token"] = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0OTY1In0.98ECEFbBQ40C52CHkrPm16jpMTKawXcg1QVqYZMqqX8";
  root["Key"] = "23635";
  root["Value"] = 22;
  String result;
  serializeJson(root, result);
  Serial.print("Will send this json: ");
  Serial.println(result);
  httpsClient.print(String("GET ") + path + 
                    "HTTP/1.1\r\n" +
                    "Host: " + host +
                    "\r\n" + "Connection: close\r\n\r\n"+result+"\r\n");
  while (httpsClient.connected()) {
    String line = httpsClient.readStringUntil('\n');
    if (line == "\r") {
      break;
    }
  }
  while (httpsClient.available()) {
    datarx += httpsClient.readStringUntil('\n');
  }
  Serial.println(datarx);
  datarx = "";
  delay(10000);
}


String url = "https://circusofthings.com/GetTimelinesViewPanel?Token=eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0OTY1In0.98ECEFbBQ40C52CHkrPm16jpMTKawXcg1QVqYZMqqX8&Ini=1615071600000&End=1615103483000&IdViewPanel=1358";

void loop() {
  Serial.print("Free Heep");
  Serial.println(ESP.getFreeHeap());
  
  timeClient.update();
  HTTPClient http;
  BearSSL::WiFiClientSecure *client = new BearSSL::WiFiClientSecure();
//  BearSSL::CertStore certStore;
//  int numCerts = certStore.initCertStore(SPIFFS, "/certs.idx", "/certs.ar");
//  client->setCertStore(&certStore);
  client->setInsecure();
//  client->setFingerprint("xxx");
//  Serial.println(numCerts);
  Serial.print("Free Heep");
  Serial.println(ESP.getFreeHeap());
  http.begin(dynamic_cast<WiFiClient&>(*client), "https://circusofthings.com/WriteValue");

  StaticJsonDocument<300> jsonDoc;
  JsonObject root = jsonDoc.to<JsonObject>();
  root["Token"] = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0OTY1In0.98ECEFbBQ40C52CHkrPm16jpMTKawXcg1QVqYZMqqX8";
  root["Key"] = "23635";
  root["Value"] = 22;
  String result;
  serializeJson(root, result);
  Serial.print("Will send this json: ");
  Serial.println(result);
  Serial.print("Free Heep");
  Serial.println(ESP.getFreeHeap());
  int httpCode = http.PUT(result);
  Serial.print("Free Heep");
  Serial.println(ESP.getFreeHeap());
  Serial.println(httpCode);
  // Serial.println(http.getString());
  delay(10000);
  
}
