#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include <CertStoreBearSSL.h>
#include <Ticker.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include <Adafruit_BMP280.h>
#include "user_interface.h"
#include "osapi.h"
#include "ets_sys.h"
#include "AsyncJson.h"
#include "ArduinoJson.h"
#include "uFire_SHT20.h"
#include <ESPAsyncWebServer.h>     //Local WebServer used to serve the configuration portal
#include <ESPAsyncWiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <TimeLib.h> // https://github.com/PaulStoffregen/Time
// previousMidnight
//nextMidnight
BearSSL::WiFiClientSecure secureClient;

// https://github.com/rjwats/esp8266-react
uFire_SHT20 sht20;
DNSServer dns;
AsyncWebServer server(80);
AsyncWiFiManager wifiManager(&server, &dns);
WiFiUDP ntpUDP;
Adafruit_BMP280 bmp;
Ticker ticker;
HTTPClient httpClient;
#define DEVELOPMENT true

#define WINTER_MODE 1
#define SUMMER_MODE 2
#define INACTIVE_MODE 0
#define AUTO_MODE 3
#define greenDiodePin 14
#define redDiodePin 12
#define btnInputPin 16
#define pwmPin 15
#define zerocrossPin 13
#define relayPin 3
#define powerSensorPin 2
#define button_cooldown_ms 1000
#define button_hold_threshold 3000
#define updatingSensorStatsInterval 60000
#define syncForecastInterval 43200000 // 12 hours
#define syncForecastTolerateLastSuccessFor 172800000 // 2 days
#define ventilatorCallibrationDown 3701 // 200 min
#define ventilatorCallibrationUp 5801 // 7200 max
#define enablementTresholdMs 4000
#define WifiShutdownCooldown 1800000
#define restartPath "/restarts"
#define stopPath "/stops"
#define averageValuesCount 7
#define averageReadingInterval 2000
#define diodeDimmingCooldown 600000 // 10 minutes
#define warmingUpTime 40000 // 1 minute 
#define temporaryDisabledProgrammeDuration 3600000 // 1 hour
#define temperatureDownTolerationProgramme 2
#define manual100ProgrammeDuration 300000 // 5 minutes
#define relayCooldown 10000
#define DEFAULT_SSID "SolarAirCollector"
#define DEFAULT_PASSWORD "5498189851"
#define TREND_MONITOR_ACTIVE_UP_TO_TEMP 70
#define WiFiConnectingTicking 1
#define AboutToChangeEnablementRedTicking 2
#define AboutToChangeEnablementGreenTicking 3
#define WarmingUpTicking 4
#define TrialTicking 5
#define DeviceEnabledWifiOnTicking 6
#define DeviceEnabledWifiOffTicking 7
#define DeviceDisabledWifiOnTicking 8
#define DeviceDisabledWifiOffTicking 9
#define TempDeviceDisabledTicking 10
#define Manual100Ticking 11
#define ErrorTicking 12
#define EnablementRed 13
#define EnablementGreen 14
#define CriticalTicking 15
#define ConditionsMet 16
// todo document
#define SummerDeviceEnabledWifiOnTicking 17
#define SummerDeviceEnabledWifiOffTicking 18
#define AutoProgrammeOnTicking 19

byte programCode = 0;
byte restarts = 0;
void wifiConnected();
void handleRoot();
void handleApiState(AsyncWebServerRequest *request);
void handleGetApiConfiguration(AsyncWebServerRequest *request);
boolean isOverlapping(short a, short b, short c, short d);
boolean isButtonPressed();
void restart();
void sendFile(const char* filename);
float average(short * array, float len);
unsigned long aNow;

byte confReadings = 0;
boolean confWriting = false;
boolean confReadLock() {
  confReadings++;
  if (confWriting) {
    confReadings--;
    return false;
  }
  return true;
}

void confReadUnlock() {
  confReadings--;
}
void confWriteLock() {
  confWriting = true;
  while (confReadings > 0) {
    delay(1);
  }
}
void confWriteUnlock() {
  confWriting = false;
}

void setCors(AsyncWebServerResponse *response) {
  response->addHeader("Access-Control-Allow-Origin", "*");
}
void setCache(AsyncWebServerResponse *response) {
  response->addHeader("Cache-Control", "public, max-age=31536000, immutable");
}
byte httpsRequestInProgress = 0;
void forceLock() {
  httpsRequestInProgress++;
}
boolean lockHttps(unsigned long timeoutMs) {
  if (timeoutMs == 0) {
    if (httpsRequestInProgress == 0) {
      httpsRequestInProgress++;
      return true;
    }
    return false;
  }
  unsigned long started = millis();
  while (millis() - started < timeoutMs) {
    if (httpsRequestInProgress == 0) {
      httpsRequestInProgress++;
      return true;
    }
    delay(1);
  }
  return false;
}
void unlockHttps() {
  httpsRequestInProgress--;
}

class Rule {
  public:
    byte temperature = 0;
    byte percentage = 0;
};

class Rules {
  public:
    Rule* rules[5];
    byte count = 0;
};

struct Monitoring {
  char feed[30] = "";
  char apikey[50] = "";
};

struct ConfigurationData {
  char name[30] = "";
  short insideTempCalibration = -15;
  byte mode = INACTIVE_MODE;
  short trendingDown = 0;
  short winterMaxInsideTemp = 0;
  short summerMinInsideTemp = 0;
  Rules *winterOnRules;
  Rules *summerOnRules;
  Monitoring *monitoring;
  short autoWinterStart = 0;
  short autoWinterEnd = 0;
  short autoSummerStart = 0;
  short autoSummerEnd = 0;
  char weatherApiKey[33] = "";
  char lat[6] = "";
  char lon[6] = "";
};

byte validateCommonSense(ConfigurationData * data);
byte getFileValue(const char* filename) {
  if (SPIFFS.exists(filename)) {
    File file = SPIFFS.open(filename, "r");
    if (!file) {
      return 0;
    }
    char buff[4] = "";
    int pos = 0;
    while (file.available() && pos < 3) {
      char c = file.read();
      if (isDigit(c)) {
        buff[pos] = c;
        pos++;
      } else {
        break;
      }
    }
    buff[pos] = 0;
    file.close();
    return atoi(buff);
  }
  return 0;
}

void writeFileValue(const char* filename, byte value) {
  if (SPIFFS.exists(filename)) {
    SPIFFS.remove(filename);
    delay(20);
  }
  File file = SPIFFS.open(filename, "w");
  if (!file) {
    return;
  }
  char buff[4] = "";
  sprintf(buff, "%d", value);
  for (short i = 0; i < 4; i++) {
    file.print(buff[i]);
  }
  file.close();
}

byte incrementInFile(const char* filename) {
  byte result = getFileValue(filename) + 1;
  writeFileValue(filename, result);
  return result;
}

void incrementRestartCounter() {
  restarts = incrementInFile(stopPath);
}

boolean bmp280initialized = false;
class ResetSensorPower {
  private:
    boolean reset = false;
  public:
    void resetIt() {
      this->reset = true;
    }

    void setup() {
      pinMode(powerSensorPin, OUTPUT);
      digitalWrite(powerSensorPin, HIGH);
    }
    void act() {
      if (this->reset) {
        this->reset = false;
        bmp280initialized = false;
        digitalWrite(powerSensorPin, LOW);
        delay(5000);
        digitalWrite(powerSensorPin, HIGH);
      }
    }
};

ResetSensorPower * resetSensorPower = new ResetSensorPower();
class Source {
  public:
    virtual float getValue() = 0;
    virtual short getToleration() = 0;
};

class BMP280Source: public Source {
  private:
    short calibration = 0;
  public:
    void setCalibration(short val) {
      this->calibration = val;
    }
    float getCalibration() {
      return this->calibration / ((float)10);
    }
    float getValue() {
      if (initBmp280IfNeeded()) {
        delay(15);
        float temp = bmp.readTemperature();
        if (temp < 80 && temp > -20) {
          return temp + this->getCalibration();
        }
      }
      return -100;
    }

    short getToleration() {
      return 5;
    }
};
BMP280Source * bmp280Source = new BMP280Source();

class MyConfiguration {
  private:
    ConfigurationData * data = {};
    boolean validate(DynamicJsonDocument c, ConfigurationData *out) {
      const char* name = c["n"].as<const char*>();
      if (strlen(name) > 29) {
        return false;
      }
      strncpy(out->name, name, 30);
      out->mode = c["mo"].as<byte>();

      out->insideTempCalibration = c["c"].as<short>();
      out->trendingDown = c["td"].as<unsigned short>();
      if (out->trendingDown < 60 || out->trendingDown > 1800) {
        return false;
      }
      
      out->autoWinterStart = c["aws"].as<short>();
      if (out->autoWinterStart < 0 || out->autoWinterStart > 365) {
        return false;
      }
      out->autoWinterEnd = c["awe"].as<short>();
      if (out->autoWinterEnd < 0 || out->autoWinterEnd > 365) {
        return false;
      }
      out->autoSummerStart = c["ass"].as<short>();
      if (out->autoSummerStart < 0 || out->autoSummerStart > 365) {
        return false;
      }
      out->autoSummerEnd = c["ase"].as<short>();
      if (out->autoSummerEnd < 0 || out->autoSummerEnd > 365) {
        return false;
      }
      if (isOverlapping(out->autoSummerStart,out->autoSummerEnd,out->autoWinterStart,out->autoWinterEnd)) {
        return false;
      }
      
      out->winterMaxInsideTemp = c["wmit"].as<short>();
      if (out->winterMaxInsideTemp < 0 || out->winterMaxInsideTemp > 1000) {
        return false;
      }

      out->summerMinInsideTemp = c["smit"].as<short>();
      if (out->summerMinInsideTemp < 0 || out->summerMinInsideTemp > 1000) {
        return false;
      }
      Monitoring * monitoring = new Monitoring();
      out->monitoring = monitoring;
      const char* apikey = c["m"]["k"].as<const char*>();
      if (strlen(apikey) > 49) {
        return false;
      }
      strncpy(monitoring->apikey, apikey, 50);
      const char* feed = c["m"]["f"].as<const char*>();
      if (strlen(feed) > 29) {
        return false;
      }
      strncpy(monitoring->feed, feed, 30);
      int winterOnRulesSize = c["wor"].size();
      if (winterOnRulesSize > 5) {
        return false;
      }

      const char* weatherApiKey = c["w"].as<const char*>();
      if (strlen(weatherApiKey) > 33) {
        return false;
      }
      strncpy(out->weatherApiKey, weatherApiKey, 33);

      const char* lat = c["lat"].as<const char*>();
      if (strlen(lat) > 5) {
        return false;
      }
      strncpy(out->lat, lat, 6);

      const char* lon = c["lon"].as<const char*>();
      if (strlen(lon) > 5) {
        return false;
      }
      strncpy(out->lon, lon, 6);

      Rules * onRules = new Rules();
      onRules->count = byte(winterOnRulesSize);
      out->winterOnRules = onRules;
      for (short i = 0; i < winterOnRulesSize; i++) {
        Rule * rule = new Rule();
        rule->temperature = c["wor"][i]["t"].as<byte>();
        rule->percentage = c["wor"][i]["p"].as<byte>();
        if (rule->percentage < 0 || rule->percentage > 100) {
          return false;
        }
        if (rule->temperature < 0 || rule->temperature > 100) {
          return false;
        }
        onRules->rules[i] = rule;
        if (i != 0) {
          Rule * previous = onRules->rules[i - 1];
          if (previous->temperature > (rule->temperature - 3)) {
            return false;
          }
        }
      }
      int summerOnRulesSize = c["sor"].size();
      if (summerOnRulesSize > 5) {
        return false;
      }
      Rules * summerOnRules = new Rules();
      summerOnRules->count = byte(summerOnRulesSize);
      out->summerOnRules = summerOnRules;
      for (short i = 0; i < summerOnRulesSize; i++) {
        Rule * rule = new Rule();
        rule->temperature = c["sor"][i]["t"].as<byte>();
        rule->percentage = c["sor"][i]["p"].as<byte>();
        if (rule->percentage < 0 || rule->percentage > 100) {
          return false;
        }
        if (rule->temperature < 0 || rule->temperature > 100) {
          return false;
        }
        summerOnRules->rules[i] = rule;
        if (i != 0) {
          Rule * previous = summerOnRules->rules[i - 1];
          if (previous->temperature < (rule->temperature + 3)) {
            return false;
          }
        }
      }
      return true;
    }

    boolean saveJson(DynamicJsonDocument doc) {
      ConfigurationData * newData = new ConfigurationData();
      if (!this->validate(doc, newData)) {
        return false;
      }
      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        return false;
      }
      serializeJson(doc, configFile);
      configFile.close();
      ConfigurationData * oldData = this->data;
      boolean hasBeenSet = this->dataSet;
      this->data = newData;
      this->dataSet = true;
      bmp280Source->setCalibration(newData->insideTempCalibration);
      if (hasBeenSet) {
        noInterrupts();
        confWriteLock();
        for (int i = 0; i < oldData->winterOnRules->count; i++) {
          delete oldData->winterOnRules->rules[i];
        }
        delete oldData->winterOnRules;
        delete oldData->monitoring;
        delete oldData;
        confWriteUnlock();
        interrupts();
      }
      return true;
    }
    boolean loadJson(DynamicJsonDocument *doc) {
      File configFile = SPIFFS.open("/config.json", "r");
      if (!configFile) {
        return false;
      }
      deserializeJson(*doc, configFile);
      configFile.close();
      return true;
    }
  public:
    boolean dataSet = false;
    ConfigurationData* getData() {
      return this->data;
    }

    boolean changeProperty(const char* name, short value) {
      DynamicJsonDocument doc(1024);
      if (this->loadJson(&doc)) {
        doc[name] = value;
        return this->saveJson(doc);
      }
      return false;
    }

    boolean save(JsonVariant &json) {
      return this->saveJson(json);
    }

    void setup() {
      DynamicJsonDocument doc(1024);
      if (this->loadJson(&doc)) {
        ConfigurationData * data = new ConfigurationData();
        if (!this->validate(doc, data)) {
          Serial.println("Cannot load config.json");
          return;
        }
        bmp280Source->setCalibration(data->insideTempCalibration);
        this->data = data;
        this->dataSet = true;
      }
    }

};

MyConfiguration * configuration = new MyConfiguration();


boolean initBmp280IfNeeded() {
  if (bmp280initialized) {
    return true;
  }
  bmp280initialized = bmp.begin();
  if (bmp280initialized) {
    bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,
                    Adafruit_BMP280::SAMPLING_X2,
                    Adafruit_BMP280::SAMPLING_X16,
                    Adafruit_BMP280::FILTER_X16,
                    Adafruit_BMP280::STANDBY_MS_500);
  }
  return bmp280initialized;
};


int I2C_ClearBus() {
#if defined(TWCR) && defined(TWEN)
  TWCR &= ~(_BV(TWEN)); //Disable the Atmel 2-Wire interface so we can control the SDA and SCL pins directly
#endif
  // https://www.nxp.com/docs/en/user-guide/UM10204.pdf - i2c specification document
  // http://www.forward.com.au/pfod/ArduinoProgramming/I2C_ClearBus/index.html
  pinMode(SDA, INPUT_PULLUP); // Make SDA (data) and SCL (clock) pins Inputs with pullup.
  pinMode(SCL, INPUT_PULLUP);

  delay(2500);  // Wait 2.5 secs. This is strictly only necessary on the first power
  // up of the DS3231 module to allow it to initialize properly,
  // but is also assists in reliable programming of FioV3 boards as it gives the
  // IDE a chance to start uploaded the program
  // before existing sketch confuses the IDE by sending Serial data.

  boolean SCL_LOW = (digitalRead(SCL) == LOW); // Check is SCL is Low.
  if (SCL_LOW) { //If it is held low Arduno cannot become the I2C master.
    return 1; //I2C bus error. Could not clear SCL clock line held low
  }

  boolean SDA_LOW = (digitalRead(SDA) == LOW);  // vi. Check SDA input.
  int clockCount = 20; // > 2x9 clock

  while (SDA_LOW && (clockCount > 0)) { //  vii. If SDA is Low,
    clockCount--;
    // Note: I2C bus is open collector so do NOT drive SCL or SDA high.
    pinMode(SCL, INPUT); // release SCL pullup so that when made output it will be LOW
    pinMode(SCL, OUTPUT); // then clock SCL Low
    delayMicroseconds(10); //  for >5uS
    pinMode(SCL, INPUT); // release SCL LOW
    pinMode(SCL, INPUT_PULLUP); // turn on pullup resistors again
    // do not force high as slave may be holding it low for clock stretching.
    delayMicroseconds(10); //  for >5uS
    // The >5uS is so that even the slowest I2C devices are handled.
    SCL_LOW = (digitalRead(SCL) == LOW); // Check if SCL is Low.
    int counter = 20;
    while (SCL_LOW && (counter > 0)) {  //  loop waiting for SCL to become High only wait 2sec.
      counter--;
      delay(100);
      SCL_LOW = (digitalRead(SCL) == LOW);
    }
    if (SCL_LOW) { // still low after 2 sec error
      return 2; // I2C bus error. Could not clear. SCL clock line held low by slave clock stretch for >2sec
    }
    SDA_LOW = (digitalRead(SDA) == LOW); //   and check SDA input again and loop
  }
  if (SDA_LOW) { // still low
    return 3; // I2C bus error. Could not clear. SDA data line held low
  }

  // else pull SDA line low for Start or Repeated Start
  pinMode(SDA, INPUT); // remove pullup.
  pinMode(SDA, OUTPUT);  // and then make it LOW i.e. send an I2C Start or Repeated start control.
  // When there is only one I2C master a Start or Repeat Start has the same function as a Stop and clears the bus.
  /// A Repeat Start is a Start occurring after a Start with no intervening Stop.
  delayMicroseconds(10); // wait >5uS
  pinMode(SDA, INPUT); // remove output low
  pinMode(SDA, INPUT_PULLUP); // and make SDA high i.e. send I2C STOP control.
  delayMicroseconds(10); // x. wait >5uS
  pinMode(SDA, INPUT); // and reset pins as tri-state inputs which is the default state on reset
  pinMode(SCL, INPUT);
  return 0; // all ok
}
void resetI2C() {
  Serial.println("resetting I2C");
  int rtn = I2C_ClearBus();
  if (rtn != 0) {
    Serial.println(F("I2C bus error. Could not clear"));
    if (rtn == 1) {
      Serial.println(F("SCL clock line held low"));
    } else if (rtn == 2) {
      Serial.println(F("SCL clock line held low by slave clock stretch"));
    } else if (rtn == 3) {
      Serial.println(F("SDA data line held low"));
    }
  } else { // bus clear
    // re-enable Wire
    // now can start Wire Arduino master
    Serial.println("setup finished");
    //    Wire.setClock(100000L);
    Wire.begin();
    bmp280initialized = false;
  }
}

class TemperatureSHT20TempHumSource: public Source {
  public:
    float getValue() {
      delay(15);
      float temperature = sht20.temperature();
      if (temperature > 125 || temperature < -80) {
        return -100;
      }
      return temperature;
    }
    short getToleration() {
      return 5;
    }
};


class HumiditySHT20TempHumSource: public Source {
  public:
    float getValue() {
      delay(15);
      float result = sht20.humidity();
      if (result > 100 || result < 0) {
        return -100;
      }
      return result;
    }
    short getToleration() {
      return 15;
    }
};

class AverageDecorator {
  private:
    Source *source;
    short values[7] = {0, 0, 0, 0, 0, 0, 0};
    short avg = 0;
    short actual = 0;
    boolean initialized = false;
    boolean error = false;
    unsigned short errorsInRow = 0;
    unsigned short index = 0;
    byte warnings = 0;
    byte initSuccessLoopCount = 0;

    short readSingle() {
      float newVal = this->source->getValue();
      if (newVal == -100) {
        this->error = true;
        return NAN;
      }
      return (short) (newVal * 100);
    }

    boolean isInValidRange(short value) {
      short toleration = this->source->getToleration() * 100;
      if (value > (this->avg + toleration) || value < (this->avg - toleration)) {
        return false;
      }
      return true;
    }

    void addWarnReading() {
      this->errorsInRow++;
      this->warnings++;
    }
    void addErrorReading() {
      this->errorsInRow++;
    }
    void startInitializing () {
      this->initialized = false;
      this->initSuccessLoopCount = 0;
      this->index = 1;
    }

    void handleUnitialized(short newVal) {
      if (this->initSuccessLoopCount == 0) {
        this->takeReading(newVal);
      } else {
        if (this->isInValidRange(newVal)) {
          this->takeReading(newVal);
        } else {
          this->addWarnReading();
        }
      }
      if (this->index == 0) {
        this->initSuccessLoopCount++;
      }
      if (this->initSuccessLoopCount >= 2) {
        this->initialized = true;
      }
    }

    void handleNormal(short newVal) {
      if (this->isInValidRange(newVal)) {
        this->takeReading(newVal);
      } else {
        this->addWarnReading();
      }
      if (errorsInRow > 5) {
        this->startInitializing();
      }
    }
  public:
    AverageDecorator(Source *source) {
      this->source = source;
    }

    void setActual(float newValue) {
      for (short i = 0; i < averageValuesCount; i++) {
        this->values[i] = (short) (newValue * 100);
      }
      this->actual = (short) (newValue * 100);
      this->avg = (short) average(this->values, averageValuesCount);
    }

    void setup() {

    }

    unsigned short getErrors() {
      return this->errorsInRow;
    }

    byte getWarnings() {
      return this->warnings;
    }

    float getAverage() {
      if (this->initialized) {
        return ((float)this->avg) / 100;
      }
      return NAN;
    }

    float getValue() {
      if (this->initialized) {
        return ((float)this->actual) / 100;
      }
      return NAN;
    }
    void takeReading(short newval) {
      this->errorsInRow = 0;
      this->values[this->index] = newval;
      this->actual = newval;
      this->avg = (short) average(this->values, averageValuesCount);
      this->index = (this->index + 1) % averageValuesCount;
    }

    void doReading() {
      this->error = false;
      short newVal = this->readSingle();
      if (!this->error) {
        if (this->initialized) {
          this->handleNormal(newVal);
        } else {
          this->handleUnitialized(newVal);
        }
      } else {
        addErrorReading();
        if (this->initialized && this->errorsInRow > 5) {
          this->startInitializing();
        }
      }
      if (this->errorsInRow != 0) {
        float remaining = errorsInRow % 40;
        if (remaining == 20) {
          resetI2C();
        }
        if (remaining == 0) {
          resetSensorPower->resetIt();
        }
        if (errorsInRow % 64000 == 0) {
          restart();
        }
      }
    }
};


class Diode {
  private:
    byte previous = 0; // 0 nothing, 1 green, 2 red
    byte tickingConf[5] = {0, 0, 0, 0, 0};
    byte tickingIndex = 0;
    byte priority  = 0;
    byte tickingSize = 0;
    byte tickingName = 0;
  public:
    Diode() {}

    void setup() {
      pinMode(greenDiodePin, OUTPUT);
      pinMode(redDiodePin, OUTPUT);
    }
    void orangeOn() {
      if (this->previous != 3) {
        digitalWrite(redDiodePin, HIGH);
        digitalWrite(greenDiodePin, HIGH);
        this->previous = 3;
      }
    }
    void greenOn() {
      if (this->previous != 1) {
        digitalWrite(redDiodePin, LOW);
        digitalWrite(greenDiodePin, HIGH);
        this->previous = 1;
      }
    }
    void redOn() {
      if (this->previous != 2) {
        digitalWrite(greenDiodePin, LOW);
        digitalWrite(redDiodePin, HIGH);
        this->previous = 2;
      }
    }

    void off() {
      if (this->previous != 0) {
        digitalWrite(greenDiodePin, LOW);
        digitalWrite(redDiodePin, LOW);
        this->previous = 0;
      }
    }
    // priority,
    // - 0 no important - can be dimmed
    // - 1 can not be dimmed
    // - 2 can not be dimmed nor override until setPriority is called
    void configureTicking(byte tickingName, byte tickingSize, const byte ticking[5], byte priority) {
      if (tickingName != this->tickingName && this->priority != 2) {
        this->priority = priority;
        this->detachTicking();
        this->tickingName = tickingName;
        for (byte i = 0; i < tickingSize; i++) {
          this->tickingConf[i] = ticking[i];
        }

        this->tickingIndex = 0;
        this->tickingSize = (tickingSize - 1) + ticking[0];
        ticker.attach_ms(100, [this]() {
          this->tick();
        });
      }
    }
    void setPriority(byte priority) {
      this->priority = priority;
    }

    void detachTicking() {
      if (this->tickingSize != 0) {
        ticker.detach();
        this->tickingSize = 0;
        this->tickingName = 0;
      }
    }
    void tick() {
      if (this->priority != 2 && isButtonPressed()) {
        this->redOn();
        return;
      }
      this->tickingIndex = (this->tickingIndex + 1) % (this->tickingSize);
      byte value = 0;
      if (this->tickingIndex < (this->tickingSize - this->tickingConf[0])) {
        value = this->tickingConf[this->tickingIndex + 1];
      }
      if (value == 0) {
        this->off();
      } else if (value == 1) {
        this->greenOn();
      } else if (value == 3) {
        this->orangeOn();
      } else {
        this->redOn();
      }
    }
};



Diode * diode = new Diode();
AverageDecorator * insideTemperatureSensor = new AverageDecorator(bmp280Source);

TemperatureSHT20TempHumSource * temperatureSht20Source = new TemperatureSHT20TempHumSource();
HumiditySHT20TempHumSource * humiditySht20Source = new HumiditySHT20TempHumSource();
AverageDecorator * outsideTemperatureSensor = new AverageDecorator(temperatureSht20Source);
AverageDecorator * outsideHumiditySensor = new AverageDecorator(humiditySht20Source);

class DewPointMonitor {
  private:
    short dewPoint;
    float doComputation(float h, float t) {
      float e = 2.71828;
      float powResult = pow(e, (17.67 * t) / (243.5 + t));
      float logResult = log((h / 100) * powResult);
      return (243.5 * logResult) / (17.67 - logResult);
    }
  public:
    float getDewPoint() {
      if (this->dewPoint == -20000) {
        return NAN;
      }
      return ((float) this->dewPoint) / 100;
    };

    void computeDewPoint() {
      float outsideTemperature = outsideTemperatureSensor->getAverage();
      float outsideHumidity = outsideHumiditySensor->getAverage();
      if (!isnan(outsideTemperature) && !isnan(outsideHumidity)) {
        this->dewPoint = (short) (doComputation(outsideHumidity, outsideTemperature) * 100);
      } else {
        this->dewPoint = -20000;
      }
    }
};

DewPointMonitor * dewPointMonitor = new DewPointMonitor();

class TrendMonitor {
  private:
    unsigned long trendingDownSince = aNow;
    short lastTemperature;
    short trendingDownSeconds;
    float reset() {
      this->trendingDownSince = aNow;
    }
  public:
    void updateTrend() {
      if (!configuration->dataSet) {
        this->reset();
        return;
      }
      if (confReadLock()) {
        ConfigurationData * data = configuration->getData();
        this->trendingDownSeconds = data->trendingDown;
        confReadUnlock();
      } else {
        this->reset();
        return;
      }
      float outsideTemperature = outsideTemperatureSensor->getAverage();
      if (isnan(outsideTemperature)) {
        this->reset();
      } else {
        if (isnan(this->lastTemperature)) {
          this->lastTemperature = (short) (outsideTemperature * 100);
        }
        if (outsideTemperature > TREND_MONITOR_ACTIVE_UP_TO_TEMP) {
          this->reset();
        } else {
          float lastTemp = ((float) this->lastTemperature) / 100;
          if (outsideTemperature > lastTemp) {
            this->reset();
          }
        }
        this->lastTemperature = (short) (outsideTemperature * 100);
      }
    }
    boolean isTrendDown() {
      unsigned long trendingDown = aNow - this->trendingDownSince;
      return trendingDown > (((float)this->trendingDownSeconds) * 1000);
    }
};
TrendMonitor * trendMonitor = new TrendMonitor();
class Programme {
  public:
    virtual int getDescription(char dest[80]) = 0;
    virtual void onStart() = 0;
    virtual boolean handleClick(short times) = 0;
    virtual void handleHold(int duration_ms, boolean finished) = 0;
    virtual byte getPower(ConfigurationData* data) = 0;
    virtual void configureTicking() = 0;
    virtual boolean isValid() = 0;
    virtual boolean canForce() = 0;
    virtual byte getCode() = 0;
};

class DisabledProgramme: public Programme {
  private:
    void configureDiode() {
      if (WiFi.status() == WL_CONNECTED) {
        byte ticking[] = {35, 2, 0, 2};
        diode->configureTicking(DeviceDisabledWifiOnTicking, 4, ticking, 0);
      } else {
        byte ticking[] = {35, 0, 0, 2};
        diode->configureTicking(DeviceDisabledWifiOffTicking, 4, ticking, 0);
      }
    }
  public:
    byte getCode() {
      return 90;
    }
    boolean canForce() {
      return false;
    }
    int getDescription(char dest[80]) {
      strcpy(dest, "Neaktivní mód");
      return 13;
    }

    void onStart() {
      this->configureDiode();
    }
    boolean handleClick(short times) {
      return false;
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    void configureTicking() {
      this->configureDiode();
    }
    byte getPower(ConfigurationData* data) {
      return 0;
    }
    boolean isValid() {
      if (!configuration->dataSet) {
        return false;
      }
      if (confReadLock()) {
        boolean result = configuration->getData()->mode == INACTIVE_MODE;
        confReadUnlock();
        return result;
      }
      return true;
    }
};

class InitialWarmingUpProgramme: public Programme {
  private:
    unsigned long startedWaiting = NAN;
    void configureDiode() {
      byte ticking[] = {2, 3, 3, 0, 3};
      diode->configureTicking(WarmingUpTicking, 5, ticking, 1);
    }
  public:
    byte getCode() {
      return 70;
    }
    boolean canForce() {
      return true;
    }
    int getDescription(char dest[80]) {
      if (isnan(this->startedWaiting)) {
        return sprintf(dest, "Zahřívání...");
      } else {
        int waitingFor = aNow - this->startedWaiting;
        int remaining = (warmingUpTime - waitingFor) / 1000;
        return sprintf(dest, "Zahřívání... Zbývá %d sekund", remaining);
      }
    }
    void onStart() {
      this->configureDiode();
      this->startedWaiting = aNow;
    }
    boolean handleClick(short times) {
      return false;
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    void configureTicking() {
      
    }
    byte getPower(ConfigurationData* data) {
      return 0;
    }

    boolean isValid() {
      if (isnan(this->startedWaiting)) {
        return false;
      }
      boolean valid = aNow - this->startedWaiting < warmingUpTime;
      if (!valid) {
        this->startedWaiting = NAN;
      }
      return valid;
    }
};

class TrialProgramme: public Programme {
  private:
    boolean valid = false;
    unsigned long started = 0;
    int durationMillis = 60000;
    byte power = 0;

    void configureDiode() {
      byte ticking[] = {2, 1, 0, 0, 3};
      diode->configureTicking(TrialTicking, 5, ticking, 1);
    }

  public:
    byte getCode() {
      return 100;
    }
    boolean canForce() {
      return true;
    }
    void onStart() {
      this->valid = true;
      this->started = aNow;
    }

    void setProps(byte power, int duration) {
      this->power = power;
      this->durationMillis = duration;
      this->started = aNow;
      this->configureDiode();
    }

    void invalidate() {
      this->valid = false;
    }

    boolean handleClick(short times) {
      this->valid = false;
      return false;
    }

    int getDescription(char dest[80]) {
      int waitingFor = aNow - this->started;
      int remaining = (this->durationMillis - waitingFor) / 1000;
      return sprintf(dest, "Manuální provoz na %d%%. Zbývá %d sekund", this->power, remaining);
    }

    void handleHold(int duration_ms, boolean finished) {

    }
    void configureTicking() {
      this->configureDiode();
    }
    byte getPower(ConfigurationData * data) {
      return this->power;
    }

    int getDuration() {
      return this->durationMillis;
    }

    boolean isValid() {
      if (!this->valid) {
        return false;
      }
      this->valid = aNow - this->started < this->durationMillis;
      return this->valid;
    }
};

int translateCode(byte code, char dest[50]) {
  if (code == 51) {
    strcpy(dest, "Nevalidní hodnota z venkovního čidla teploty");
    return 45;
  } else if (code == 52) {
    strcpy(dest, "Nevalidní hodnota z vnitřního čidla teploty");
    return 44;
  } else if (code == 53) {
    strcpy(dest, "Nevalidní hodnota z venkovního čidla vlhkosti");
    return 46;
  }
};

class TimeProvider {
  public:
    boolean isTimeSet() {
      return timeStatus() != timeNotSet;
    }

    void updateTime(unsigned long unixTimestamp, int offset) {
      setTime(unixTimestamp);
      adjustTime(offset);
    }
};
TimeProvider * timeProvider = new TimeProvider();

class WeatherForecast {
  private:
    float feelsLikeToday = 0;
    short lastStatusCode = -100;
    unsigned long last_retrival;
    unsigned long last_success;

    void getUrl(char url[]) {
      sprintf(url, "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&exclude=minutely,hourly,alerts&units=metric&appid=%s", configuration->getData()->lat, configuration->getData()->lon, configuration->getData()->weatherApiKey);
    }

    boolean sync() {
      char url[200] = "";
      this->getUrl(url);
      HTTPClient httpClientForecast;
      httpClientForecast.begin(dynamic_cast<WiFiClient&>(secureClient), url);
      this->lastStatusCode = httpClientForecast.GET();
      if (this->lastStatusCode >= 200 && this->lastStatusCode < 300) {
        int contentLength = httpClientForecast.getSize();
        if (contentLength == -1) {
          contentLength = 6000;
        }
        DynamicJsonDocument doc(contentLength);
        deserializeJson(doc, httpClientForecast.getStream());
        int offset = doc["timezone_offset"].as<int>();
        unsigned long unixTime = doc["current"]["dt"].as<unsigned long>();
        timeProvider->updateTime(unixTime, offset);
        this->feelsLikeToday = doc["daily"][0]["feels_like"]["day"].as<float>();
        httpClientForecast.end();
        return true;
      } else {
        httpClientForecast.end();
        return false;
      }
    }
    void doSync() {
      if (WiFi.status() == WL_CONNECTED && configuration->dataSet) {
        if (lockHttps(100)) {
          if (confReadLock()) {
            this->last_retrival = aNow;
            if (this->sync()) {
              this->last_success = aNow;
            }
            confReadUnlock();
          }
          unlockHttps();
        }
      }
    }

  public:
    int describe(char dest[40]) {
      return sprintf(dest, "Feels like %d", (int) this->feelsLikeToday);
    }
    int shouldIRun() {
      return 0;
    }
    short getStatusCode() {
      return this->lastStatusCode;
    }
    boolean hasValidForecast() {
      return !(this->lastStatusCode != -100) && (aNow - this->last_success > syncForecastTolerateLastSuccessFor);
    }
    void act() {
      if ((this->lastStatusCode == -100) || (aNow - this->last_retrival > syncForecastInterval)) {
        this->doSync();
      }
    }
};

WeatherForecast * weatherForecast = new WeatherForecast();

class WinterProgramme: public Programme {
  private:
    short currentRuleIndex = -1;
    byte error = 0;
    void configureDiode() {
      if (this->error != 0) {
        byte ticking[] = {25, 1, 1, 3, 3};
        diode->configureTicking(ConditionsMet, 5, ticking, 1);
      } else {
        if (WiFi.status() == WL_CONNECTED) {
          byte ticking[] = {35, 3, 0, 3};
          diode->configureTicking(DeviceEnabledWifiOnTicking, 4, ticking, 0);
        } else {
          byte ticking[] = {37, 3};
          diode->configureTicking(DeviceEnabledWifiOffTicking, 2, ticking, 0);
        }
      }
    }
    float markError(byte error) {
      this->error = error;
      return 0;
    }
  public:
    void onStart() {
      this->currentRuleIndex = -1;
      this->configureDiode();
    }
    byte getCode() {
      if (this->error != 0) {
        return this->error;
      }
      return 120;
    }
    boolean canForce() {
      return false;
    }
    boolean handleClick(short times) {
      return false;
    }
    int getDescription(char dest[80]) {
      if (this->error == 121) {
        strcpy(dest, "Teplota ve vnitřním prostoru byla překročena");
        return 45;
      } else if (this->error == 122) {
        strcpy(dest, "Teplota v kolektoru pomalu klesá.");
        return 34;
      } else if (this->error == 123) {
        strcpy(dest, "Rosný bod překračuje vnitřní teplotu.");
        return 36;
      } else if (this->error == 123) {
        strcpy(dest, "Rosný bod nelze spočítat.");
        return 26;
      } else {
        strcpy(dest, "Vyhřívací mód.");
        return 15;
      }
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    void configureTicking() {
      this->configureDiode();
    }
    byte getPower(ConfigurationData* data) {
      float outsideAirTemp = outsideTemperatureSensor->getAverage();
      float insideTemp = insideTemperatureSensor->getAverage();
      if (insideTemp > (data->winterMaxInsideTemp / ((float)10))) {
        return this->markError(121);
      }
      float outsideHumidity = outsideHumiditySensor->getAverage();
      float dewPoint = dewPointMonitor->getDewPoint();
      if (isnan(dewPoint)) {
        return this->markError(124);
      }
      // if (dewPoint > (insideTemp - 2)) {
      //   return this->markError(123);
      // }
      Rules * rules = data->winterOnRules;
      for (short i = (rules->count - 1); i >= 0; i--) {
        Rule * rule = rules->rules[i];
        if (outsideAirTemp >= ((float) rule->temperature)) {
          this->currentRuleIndex = i;
          byte percentage = rule->percentage;
          if (trendMonitor->isTrendDown()) {
            return this->markError(122);
          }
          this->error = 0;
          return percentage;
        }
        if (this->currentRuleIndex == i) {
          if (outsideAirTemp > (((float) rule->temperature) - temperatureDownTolerationProgramme)) {
            byte percentage = rule->percentage;
            if (trendMonitor->isTrendDown()) {
              return this->markError(122);
            }
            this->error = 0;
            return percentage;
          }
        }
      }
      this->currentRuleIndex = -1;
      this->error = 0;
      return 0;
    }
    boolean isValid() {
      if (!configuration->dataSet) {
        return false;
      }
      if (confReadLock()) {
        ConfigurationData * data = configuration->getData();
        boolean result = data->mode == WINTER_MODE;
        confReadUnlock();
        return result;
      }
      return true;
    }
};

class SummerProgramme: public Programme {
  private:
    short currentRuleIndex = -1;
    byte error = 1;
    void configureDiode() {
      if (this->error >= 113) {
        byte ticking[] = {25, 1, 1, 3, 3};
        diode->configureTicking(ConditionsMet, 5, ticking, 1);
      } else {
        if (WiFi.status() == WL_CONNECTED) {
          byte ticking[] = {35, 0, 1, 0, 1};
          diode->configureTicking(SummerDeviceEnabledWifiOnTicking, 5, ticking, 0);
        } else {
          byte ticking[] = {37, 0, 1};
          diode->configureTicking(SummerDeviceEnabledWifiOffTicking, 3, ticking, 0);
        }
      }
    }
    float markError(byte error) {
      this->error = error;
      return 0;
    }
  public:
    void onStart() {
    }
    byte getCode() {
      if (this->error != 0) {
        return this->error;
      }
      return 110;
    }
    boolean canForce() {
      return false;
    }
    boolean handleClick(short times) {
      return false;
    }
    int getDescription(char dest[80]) {
      if (this->error == 111) {
        strcpy(dest, "Bylo dosáhnuto cílové nízké teploty");
        return 36;
      } else if (this->error == 113) {
        strcpy(dest, "Rosný bod překračuje vnitřní teplotu.");
        return 38;
      } else if (this->error == 114) {
        strcpy(dest, "Nelze spočítat rosný bod.");
        return 26;
      } else if (this->error == 115) {
        strcpy(dest, "Není k dispozici předpověď počasí.");
        return 34;
      } else {
        strcpy(dest, "Chladící mód. ");
        char explanation[40];
        weatherForecast->describe(explanation);
        strcat(dest, explanation);
        return 50;
      }
    }
    void configureTicking()  {
      this->configureDiode();
    }
    byte getPower(ConfigurationData* data) {
      weatherForecast->act();
      if (weatherForecast->hasValidForecast()) {
        this->error = 115;
        return 0;
      }
      int forecastCode = weatherForecast->shouldIRun();
      if (forecastCode != 0) { 
        if (forecastCode == 1) {
          this->error = 116; // todo
          return 0;
        }
      }
      float insideTemp = insideTemperatureSensor->getAverage();
      if (insideTemp < (data->summerMinInsideTemp / ((float)10))) {
        this->markError(111);
        return 0;
      }
      float outsideHumidity = outsideHumiditySensor->getAverage();
      float dewPoint = dewPointMonitor->getDewPoint();
      if (isnan(dewPoint)) {
        return this->markError(114);
      }
      if (dewPoint > (insideTemp - 2)) {
        return this->markError(113);
      }
      float outsideAirTemp = outsideTemperatureSensor->getAverage();
      Rules * rules = data->summerOnRules;
      for (short i = (rules->count - 1); i >= 0 ; i--) {
        Rule * rule = rules->rules[i];
        if (outsideAirTemp < ((float) rule->temperature)) {
          this->currentRuleIndex = i;
          byte percentage = rule->percentage;
          this->error = 0;
          return percentage;
        }
        if (this->currentRuleIndex == i) {
          if (outsideAirTemp < (((float) rule->temperature) + temperatureDownTolerationProgramme)) {
            byte percentage = rule->percentage;
            this->error = 0;
            return percentage;
          }
        }
      }
      this->currentRuleIndex = -1;
      this->error = 0;
      return 0;
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    boolean isValid() {
      if (!configuration->dataSet) {
        return false;
      }
      if (confReadLock()) {
        ConfigurationData * data = configuration->getData();
        boolean valid = data->mode == SUMMER_MODE;
        confReadUnlock();
        return valid;
      }
      return true;
    }
};

class ErrorProgramme: public Programme {
  public:
    void onStart() {
      digitalWrite(pwmPin, LOW);
      byte ticking[] = {8, 2, 2, 2, 2};
      diode->configureTicking(CriticalTicking, 5, ticking, 1);
    }
    int getDescription(char dest[80]) {
      strcpy(dest, "Chybná konfigurace");
      return 18;
    }
    byte getCode() {
      return 80;
    }
    boolean canForce() {
      return false;
    }
    boolean handleClick(short times) {
      return false;
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    void configureTicking() {
      
    }
    byte getPower(ConfigurationData* data) {
      return 0;
    }
    boolean isValid() {
      if (configuration->dataSet) {
        return true;
      }
      return false;
    }
};

Programme * winterProgramme = new WinterProgramme();
Programme * summerProgramme = new SummerProgramme();
Programme * disabledProgramme = new DisabledProgramme();
Programme * errorProgramme = new ErrorProgramme();
Programme * warmingUpProgramme = new InitialWarmingUpProgramme();
TrialProgramme * trialProgramme = new TrialProgramme();

class AutoProgramme: public Programme {
  private:
    byte error = 0;
    Programme *actual = disabledProgramme;
    void configureDiode() {
      byte ticking[] = {35, 1, 2, 3};
      diode->configureTicking(AutoProgrammeOnTicking, 4, ticking, 0);
    }
    boolean isInInterval(int days, int intervalStart, int intervalEnd) {
      if (intervalStart < intervalEnd) {
        return (intervalStart <= days) && (days < intervalEnd);
      } else {
        return (intervalStart < days) || (days <= intervalEnd);
      }
    }
    int daysSinceYearStart() {
      int elapsedDays = day();
      int monthNow = month();
      if (monthNow > 1) elapsedDays += 31;
      if (monthNow > 2) elapsedDays += 28;
      if (monthNow > 3) elapsedDays += 31;
      if (monthNow > 4) elapsedDays += 30;
      if (monthNow > 5) elapsedDays += 31;
      if (monthNow > 6) elapsedDays += 30;
      if (monthNow > 7) elapsedDays += 31;
      if (monthNow > 8) elapsedDays += 31;
      if (monthNow > 9) elapsedDays += 30;
      if (monthNow > 10) elapsedDays += 31;
      if (monthNow > 11) elapsedDays += 30;
      if (monthNow > 12) elapsedDays += 31;
      return elapsedDays;
    }

    void assignCandidate(ConfigurationData* data) {
      if (!timeProvider->isTimeSet()) {
        this->actual = disabledProgramme;
        return;
      }
      int elapsedDays = this->daysSinceYearStart();
      
      boolean isInWinter = this->isInInterval(elapsedDays, data->autoWinterStart, data->autoWinterEnd);
      boolean isInSummer = this->isInInterval(elapsedDays, data->autoSummerStart, data->autoSummerEnd);
      if (isInWinter) {
        this->actual = winterProgramme;  
      } else if (isInSummer) {
        this->actual = summerProgramme;
      } else {
        this->actual = disabledProgramme;
      }
    }
  public:
    void onStart() {
      
    }
    byte getCode() {
      if (this->error != 0) {
        return this->error;
      }
      return 130;
    }
    boolean canForce() {
      return false;
    }
    boolean handleClick(short times) {
      return false;
    }
    int getDescription(char dest[80]) {
      if (this->error == 133) {
        strcpy(dest, "Čas není synchronizovaný");
        return 31;
      } else {
        int elapsedDays = daysSinceYearStart();

        char nested[40] = "";
        int written = this->actual->getDescription(nested);
        strcat(dest, "Auto mod: ");
        strcat(dest, nested);
        return 80;
      }
    }
    void configureTicking() {
      this->configureDiode();
    }

    byte getPower(ConfigurationData* data) {
      if (!timeProvider->isTimeSet()) {
        weatherForecast->act();
        this->error = 133;
        return 0;
      }
      this->assignCandidate(data);
      this->error = 0;
      return this->actual->getPower(data);
    }
    void handleHold(int duration_ms, boolean finished) {

    }
    boolean isValid() {
      if (!configuration->dataSet) {
        return false;
      }
      if (confReadLock()) {
        ConfigurationData * data = configuration->getData();
        boolean valid = data->mode == AUTO_MODE;
        confReadUnlock();
        return valid;
      }
      return true;
    }
};

Programme * autoProgramme = new AutoProgramme();

// https://bbs.espressif.com/viewtopic.php?t=1205 - dev 1163
short acDelayMicroseconds = 0;
void zero_crosss_timer_handler(void* pTimerArg) {
  digitalWrite(pwmPin, HIGH);
  delayMicroseconds(50);
  digitalWrite(pwmPin, LOW);
}

ETSTimer osTimer;

void ICACHE_RAM_ATTR zero_crosss_int() {
  // https://www.esp8266.com/viewtopic.php?f=13&t=2392
  // dobra stranka> https://sub.nanona.fi/esp8266/hello-world.html
  // sming hub? https://sminghub.github.io/Sming/api/annotated.html
  ets_timer_arm_new(&osTimer, acDelayMicroseconds, false, false); // repeat, millis
}

class Relay {
  private:
    unsigned long ended = 0;
    byte shutting_down = false;
  public:
    void setup() {
      pinMode(relayPin, OUTPUT);
      digitalWrite(relayPin, HIGH);
    }
    void act() {
      if (this->shutting_down) {
        if ((aNow - this->ended) > relayCooldown) {
          this->shutting_down = false;
          digitalWrite(relayPin, HIGH);
        }
      }
    }
    void disable() {
      this->ended = aNow;
      this->shutting_down = true;
    }
    void enable() {
      this->shutting_down = false;
      digitalWrite(relayPin, LOW);
    }
};
Relay * relay = new Relay();

class Ventilator {
  private:
    boolean attached = false;
    byte power = 0;
    byte previousPower = 0;
    void attach() {
      if (!this->attached) {
        attachInterrupt(zerocrossPin, zero_crosss_int, RISING);
        this->attached = true;
      }
    }
    void detach() {
      if (this->attached) {
        detachInterrupt(digitalPinToInterrupt(zerocrossPin));
        this->attached = false;
      }
    }
  public:
    void setup() {
      pinMode(pwmPin, OUTPUT);
      pinMode(zerocrossPin, INPUT_PULLUP);
    }
    void setPower(byte newPower) {
      if (newPower <= 0) {
        this->power = 0;
      } else if (newPower >= 100) {
        this->power = 100;
      } else {
        this->power = newPower;
      }
    }
    byte getPower() {
      return this->power;
    }
    void act() {
      if (this->previousPower == this->power) {
        return;
      }
      this->previousPower = this->power;
      if (this->previousPower > 0) {
        relay->enable();
        if (this->previousPower >= 100) {
          acDelayMicroseconds = map(100, 0, 100, ventilatorCallibrationUp, ventilatorCallibrationDown);
        } else {
          acDelayMicroseconds = map(this->previousPower, 0, 100, ventilatorCallibrationUp, ventilatorCallibrationDown);
        }
        this->attach();
      } else {
        this->detach();
        digitalWrite(pwmPin, LOW);
        relay->disable();
      }
    }
};

Ventilator * ventilator = new Ventilator();

class MonitoringClazz {
  private:
    void getDataUrl(char url[]) {
      sprintf(url, "https://iotplotter.com/api/v2/feed/%s", configuration->getData()->monitoring->feed);
    }

  public:
    void report() {
      if (WiFi.status() == WL_CONNECTED && configuration->dataSet) {
        if (lockHttps(100)) {
          if (confReadLock()) {
            StaticJsonDocument<512> doc;
            JsonObject data = doc.createNestedObject("data");
            float outsideTemperature = outsideTemperatureSensor->getAverage();
            if (!isnan(outsideTemperature)) {
              data["Teplota"][0]["value"] = outsideTemperature;
            }
            float outsideHumidity = outsideHumiditySensor->getAverage();
            if (!isnan(outsideHumidity)) {
              data["Vlhkost"][0]["value"] = outsideHumidity;
            }
            data["Vykon"][0]["value"] = ventilator->getPower();
            data["Code"][0]["value"] = ((float) programCode) / 10;
            float insideTemperature = insideTemperatureSensor->getAverage();
            if (!isnan(insideTemperature)) {
              data["TeplotaVnitrni"][0]["value"] = insideTemperature;
            }
            float dewPoint = dewPointMonitor->getDewPoint();
            if (!isnan(dewPoint)) {
              data["RosnyBod"][0]["value"] = dewPoint;
            }
            data["Restarts"][0]["value"] = restarts;
            data["Ok"][0]["value"] = weatherForecast->getStatusCode();
            data["Heap"][0]["value"] = ESP.getFreeHeap();
            data["Warnings"][0]["value"] = (outsideTemperatureSensor->getWarnings() + outsideHumiditySensor->getWarnings() + insideTemperatureSensor->getWarnings());
            data["Errors"][0]["value"] = (outsideTemperatureSensor->getErrors() + outsideHumiditySensor->getErrors() + insideTemperatureSensor->getErrors());
            char requestBody[512];
            serializeJson(doc, requestBody);
            char url[70] = "";
            this->getDataUrl(url);
            httpClient.begin(dynamic_cast<WiFiClient&>(secureClient), url);
            httpClient.addHeader("api-key", configuration->getData()->monitoring->apikey);
            int responseCode = httpClient.POST(requestBody);
            httpClient.end();
            confReadUnlock();
          }
          unlockHttps();
        }
      }
    }
};
MonitoringClazz * monitoring = new MonitoringClazz();

class SensorStatistics {
  private:
    unsigned long last_retrival = aNow;
  public:
    void setup() {}

    void act() {
      if (aNow - this->last_retrival > updatingSensorStatsInterval) {
        this->last_retrival = aNow;
        monitoring->report();
      }
    }
};

SensorStatistics * sensorStats = new SensorStatistics();


byte validateCommonSense(ConfigurationData * data) {
  float outsideAirTemp = outsideTemperatureSensor->getAverage();
  if (isnan(outsideAirTemp)) {
    return 51;
  }
  float insideTemp = insideTemperatureSensor->getAverage();
  if (isnan(insideTemp)) {
    return 52;
  }
  float outsideHumidity = outsideHumiditySensor->getAverage();
  if (isnan(outsideHumidity)) {
    return 53;
  }
  return 0;
}

class Orchestrator {
  private:
    Programme *actual = warmingUpProgramme;
  public:
    int getProgrammeName(char dest[80]) {
      if (programCode < 60) {
        translateCode(programCode, dest);
      } else {
        return this->actual->getDescription(dest);
      }
    }
    void setup() {
      this->actual->onStart();
    }
    void setProgramme(Programme * programme) {
      programme->onStart();
      this->actual = programme;
    }

    void assignProgramme() {
      Programme * candidate = disabledProgramme;
      if (confReadLock()) {
        ConfigurationData * data = configuration->getData();
        if (!configuration->dataSet) {
          candidate = errorProgramme;
        } else if (data->mode == WINTER_MODE) {
          candidate = winterProgramme;
        } else if (data->mode == SUMMER_MODE) {
          candidate = summerProgramme;
        } else if (data->mode == AUTO_MODE) {
          candidate = autoProgramme;
        } else {
          candidate = disabledProgramme;
        }
        confReadUnlock();
      }

      if (candidate != this->actual) {
        this->actual = candidate;
        this->actual->onStart();
      }
    }

    boolean handleClick(short times) {
      if (times == 2) {
        trialProgramme->setProps(0, temporaryDisabledProgrammeDuration);
        this->setProgramme(dynamic_cast<Programme*>(trialProgramme));
        return true;
      }
      if (times == 3) {
        trialProgramme->setProps(100, manual100ProgrammeDuration);
        this->setProgramme(dynamic_cast<Programme*>(trialProgramme));
        return true;
      }
      return this->actual->handleClick(times);
    }

    boolean handleHold(int duration_ms, boolean finished) {
      this->actual->handleHold(duration_ms, finished);
      if (duration_ms > 10000) {
        restart();
      }
      if (finished) {
        diode->setPriority(0);
        if (duration_ms > enablementTresholdMs) {
          if (configuration->dataSet) {
            configuration->changeProperty("mo", INACTIVE_MODE);
            this->assignProgramme();
          }
        }

      } else {
        if (duration_ms > enablementTresholdMs) {
          byte ticking[] = {1, 2};
          diode->configureTicking(EnablementRed, 2, ticking, 2);
        }
      }
      return finished;
    }
    void act() {
      if (!this->actual->isValid()) {
        this->assignProgramme();
      }
      if (!confReadLock()) {
        return;
      }
      ConfigurationData * data = configuration->getData();
      byte percentage = 0;
      if (this->actual->canForce()) {
        this->actual->configureTicking();
        percentage = this->actual->getPower(data);
        programCode = this->actual->getCode();
      } else {
        programCode = validateCommonSense(data);
        if (programCode == 0) {
          this->actual->configureTicking();
          percentage = this->actual->getPower(data);
          programCode = this->actual->getCode();
        } else {
          byte ticking[] = {1, 2};
          diode->configureTicking(ErrorTicking, 2, ticking, 1);
        }
      }
      confReadUnlock();
      ventilator->setPower(percentage);
    }
};
Orchestrator * orchestrator = new Orchestrator();

class Button {
  private:
    boolean last_value = false;
    unsigned long btn_pressed_time = 0;
    unsigned long btn_unpressed_time = 0;
    byte clicks = 0;

    void reset_state() {
      this->btn_pressed_time = NAN;
      this->btn_unpressed_time = NAN;
      this->clicks = 0;
    }

  public:
    void setup() {
      pinMode(btnInputPin, INPUT);
    }
    boolean isPressed() {
      return this->last_value;
    }
    void act() {
      boolean pressed = digitalRead(btnInputPin);
      if (!pressed && this->btn_pressed_time == 0) {
        return;
      }

      // press down edge
      if (pressed && !this->last_value) {
        this->btn_pressed_time = aNow;
      }

      // release edge
      int duration = (int)(aNow - this->btn_pressed_time);
      if (pressed && this->last_value && duration > ((int)button_hold_threshold)) {
        if (orchestrator->handleHold(duration, false)) {
          this->reset_state();
          this->last_value = true;
          return;
        }
      }
      if (!pressed && this->last_value) {
        if (duration > ((int)button_hold_threshold)) {
          orchestrator->handleHold(duration, true);
          this->reset_state();
          this->last_value = pressed;
          return;
        } else {
          this->clicks = this->clicks + 1;
          this->btn_unpressed_time = aNow;
        }
      }

      if (!pressed && !isnan(this->btn_unpressed_time) && (aNow - this->btn_unpressed_time > button_cooldown_ms)) {
        orchestrator->handleClick(this->clicks);
        this->reset_state();
      }
      this->last_value = pressed;
    }
    boolean read() {
      boolean value = digitalRead(btnInputPin);
      return value;
    }
};
Button * button = new Button();
boolean isButtonPressed() {
  return button->isPressed();
}

boolean restartI2CinNextLoop = false;
boolean restartInNextLoop = false;
class MyWebServer {
  public:
    void setup() {
      if (configuration->dataSet && confReadLock()) {
        WiFi.hostname(configuration->getData()->name);
        confReadUnlock();
      } else {
        WiFi.hostname("rekuperace");
      }
      byte ticking[] = {0, 1, 2, 3};
      diode->configureTicking(WiFiConnectingTicking, 4, ticking, 1);

      wifiManager.setConfigPortalTimeout(180); // 3 minutes to be configured, then continue
      wifiManager.autoConnect(DEFAULT_SSID, DEFAULT_PASSWORD);
      server.on("/a/s/", HTTP_GET, [](AsyncWebServerRequest * request) {
        handleApiState(request);
      });
      server.on("/a/conf/", HTTP_GET, [](AsyncWebServerRequest * request) {
        handleGetApiConfiguration(request);
      });
      AsyncCallbackJsonWebHandler* handler = new AsyncCallbackJsonWebHandler("/a/t", [](AsyncWebServerRequest * request, JsonVariant & json) {
        if (lockHttps(0)) {
          if (json["on"].as<int>() == 1) {
            byte power = json["power"].as<byte>();
            int duration = json["duration"].as<int>();
            trialProgramme->setProps(power, duration);
            orchestrator->setProgramme(dynamic_cast<Programme*>(trialProgramme));
          } else {
            trialProgramme->invalidate();
          }
          AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "{'msg':'done'}");
          setCors(response);
          request->send(response);
          unlockHttps();
        } else {
          AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
          setCors(response);
          request->send(response);
        }
      });

      AsyncCallbackJsonWebHandler* trialHandler = new AsyncCallbackJsonWebHandler("/a/conf", [](AsyncWebServerRequest * request, JsonVariant & json) {
        if (lockHttps(0)) {
          if (configuration->save(json)) {
            AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "{'msg':'done'}");
            setCors(response);
            request->send(response);
          } else {
            AsyncWebServerResponse *response = request->beginResponse(400, "application/json", "{'msg':'Could not parse JSON or not valid values'}");
            setCors(response);
            request->send(response);
          }
          unlockHttps();
        } else {
          AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
          setCors(response);
          request->send(response);
        }
      });
      AsyncCallbackJsonWebHandler* calibrationHandler = new AsyncCallbackJsonWebHandler("/a/cal", [](AsyncWebServerRequest * request, JsonVariant & json) {
        if (lockHttps(0)) {
          float actualTemp = json["insideTemp"].as<float>();
          float sensorTemp = insideTemperatureSensor->getValue();
          if (!isnan(sensorTemp)) {
            float originalSensorTemp = sensorTemp - bmp280Source->getCalibration();
            short calibration = (short)((actualTemp - originalSensorTemp) * 10);
            if (configuration->changeProperty("c", calibration)) {
              insideTemperatureSensor->setActual(actualTemp);
              AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "");
              setCors(response);
              request->send(response);
            } else {
              AsyncWebServerResponse *response = request->beginResponse(500, "application/json", "");
              setCors(response);
              request->send(response);
            }
          } else {
            AsyncWebServerResponse *response = request->beginResponse(500, "application/json", "Inside temperature sensor is NaN");
            setCors(response);
            request->send(response);
          }
          unlockHttps();
        } else {
          AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
          setCors(response);
          request->send(response);
        }
      });

      AsyncCallbackJsonWebHandler* actionHandler = new AsyncCallbackJsonWebHandler("/   ", [](AsyncWebServerRequest * request, JsonVariant & json) {
        if (lockHttps(0)) {
          if (json["id"].as<byte>() == 1) {
            AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "{'msg':'done'}");
            setCors(response);
            request->send(response);
            unlockHttps();
            restartInNextLoop = true;
          } else if (json["id"].as<byte>() == 2) {
            restartI2CinNextLoop = true;
            AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "{'msg':'done'}");
            setCors(response);
            request->send(response);
          } else if (json["id"].as<byte>() == 3) {
            resetSensorPower->resetIt();
            AsyncWebServerResponse *response = request->beginResponse(200, "application/json", "{'msg':'done'}");
            setCors(response);
            request->send(response);
          } else {
            AsyncWebServerResponse *response = request->beginResponse(400, "application/json", "bad request");
            setCors(response);
            request->send(response);
          }
          unlockHttps();
        } else {
          AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
          setCors(response);
          request->send(response);
        }
      });

      server.addHandler(handler);
      server.addHandler(calibrationHandler);
      server.addHandler(trialHandler);
      server.addHandler(actionHandler);
      server.onNotFound([](AsyncWebServerRequest * request) {
        if (request->method() == HTTP_OPTIONS) {
          AsyncWebServerResponse *response = request->beginResponse(204);
          setCors(response);
          setCache(response);
          request->send(response);
        } else if (request->method() == HTTP_GET) {
          AsyncWebServerResponse *response = request->beginResponse(SPIFFS, "/index.html");
          setCache(response);
          request->send(response);
        }
      });
      server.begin();
    }

    void act() {
      dns.processNextRequest();
    }
};


MyWebServer * myWebServer = new MyWebServer();

void setup() {
  Serial.begin(230400);
  SPIFFS.begin();
  delay(1000);
  aNow = millis();
  incrementRestartCounter();
  system_timer_reinit();
  configuration->setup();
  button->setup();
  diode->setup();
  myWebServer->setup();
  delay(20);
  insideTemperatureSensor->setup();
  outsideTemperatureSensor->setup();
  outsideHumiditySensor->setup();
  orchestrator->setup();
  ventilator->setup();
  sensorStats->setup();
  secureClient.setInsecure();
  httpClient.setReuse(true);
  relay->setup();
  ets_timer_setfn(&osTimer, zero_crosss_timer_handler, NULL);
  resetSensorPower->setup();
}

unsigned long last_sensor_reading = aNow;

void loop() {
  aNow = millis();
  resetSensorPower->act();
  if (restartI2CinNextLoop) {
    resetI2C();
  }
  if (restartInNextLoop) {
    restart();
  }
  sensorStats->act();
  myWebServer->act();
  if (aNow - last_sensor_reading > averageReadingInterval) {
    last_sensor_reading = aNow;
    insideTemperatureSensor->doReading();
    outsideTemperatureSensor->doReading();
    outsideHumiditySensor->doReading();
    dewPointMonitor->computeDewPoint();
    trendMonitor->updateTrend();
  }
  orchestrator->act();
  button->act();
  ventilator->act();
  relay->act();
  delay(10);
}

void handleGetApiConfiguration(AsyncWebServerRequest *request) {
  if (lockHttps(0)) {
    AsyncWebServerResponse * response = request->beginResponse(SPIFFS, "/config.json", "application/json");
    setCors(response);
    request->send(response);
    unlockHttps();
  } else {
    AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
    setCors(response);
    request->send(response);
  }
}


void handleApiState(AsyncWebServerRequest *request) {
  if (lockHttps(0)) {
    AsyncResponseStream *response = request->beginResponseStream("application/json");
    StaticJsonDocument<520> jsonDoc;
    byte mode = INACTIVE_MODE;
    byte trialPower = 0;
    if (configuration->dataSet && confReadLock()) {
      mode = configuration->getData()->mode;
      if (trialProgramme->isValid()) {
        ConfigurationData * data = configuration->getData();
        trialPower = trialProgramme->getPower(data);
      }
      confReadUnlock();
    }
    JsonObject root = jsonDoc.to<JsonObject>();
    if (timeProvider->isTimeSet()) {
      char timeBuffer[40];
      sprintf(timeBuffer, "%d/%d/%d %d:%d.%d", year(), month(), day(), hour(), minute(), second());
      root["t"] = timeBuffer;
    } else {
      root["t"] = "Čas nenastaven.";
    }
    root["p"] = ventilator->getPower();
    root["a"] = aNow;
    root["mo"] = mode;
    char description[80] = "";
    orchestrator->getProgrammeName(description);
    root["de"] = description;
    root["r"] = restarts;

    root["h"] = ESP.getFreeHeap();
    JsonObject outside = root.createNestedObject("o");
    if (trialProgramme->isValid()) {
      root["tpp"] = trialPower;
      root["tpd"] = trialProgramme->getDuration();
    }
    outside["t"] = outsideTemperatureSensor->getValue();
    outside["tAvg"] = outsideTemperatureSensor->getAverage();
    outside["tErr"] = outsideTemperatureSensor->getErrors();
    outside["tWarn"] = outsideTemperatureSensor->getWarnings();
    outside["hErr"] = outsideHumiditySensor->getErrors();
    outside["hWarn"] = outsideHumiditySensor->getWarnings();
    outside["h"] = outsideHumiditySensor->getValue();
    outside["hAvg"] = outsideHumiditySensor->getAverage();
    JsonObject inside = root.createNestedObject("i");
    inside["t"] = insideTemperatureSensor->getValue();
    inside["tAvg"] = insideTemperatureSensor->getAverage();
    inside["tErr"] = insideTemperatureSensor->getErrors();
    inside["tWarn"] = insideTemperatureSensor->getWarnings();
    serializeJson(root, *response);
    setCors(response);
    request->send(response);
    unlockHttps();
  } else {
    AsyncWebServerResponse *response = request->beginResponse(429, "application/json", "");
    setCors(response);
    request->send(response);
  }
}

void restart() {
  ventilator->setPower(0);
  ventilator->act();
  diode->detachTicking();
  if (!DEVELOPMENT) {
    diode->redOn();  
    resetI2C();
    delay(5000);
    ESP.restart();
  }
}


float average (short * values, float len) {
  int sum = 0;
  for (short i = 0 ; i < len ; i++)
    sum += values[i] ;
  return sum / len;
}

boolean isOverlapping(short a, short b, short c, short d) {
    if (a < b && c < d) {
        return a < d && c < b;
    }
    if (a > b && c > d) {
        return true;
    }
    short m = 365;
    short w0 = (b - a) % m;
    short w1 = (d - c) % m;
    return (w1 != 0 && ((c - a)%m) < w0) || (w0 != 0 && ((a - c)%m) < w1);
}
