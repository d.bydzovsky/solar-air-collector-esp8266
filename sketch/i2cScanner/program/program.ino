void setup()
{
  pinMode(A0, OUTPUT);
  Serial.begin(9600);
  Serial.println("\nA0 high low");
}
 
 
void loop()
{
  digitalWrite(A0, HIGH);
  delay(5000);           
  digitalWrite(A0, LOW);
  delay(5000);           
}
