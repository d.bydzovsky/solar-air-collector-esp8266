#define outputPin  15 
#define zerocross  13 // for boards with CHANGEBLE input pins

short acDelayMicroseconds = 200;

void ICACHE_RAM_ATTR zero_crosss_int() {
  delayMicroseconds(acDelayMicroseconds);
  digitalWrite(outputPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(outputPin, LOW);
}

void setup() {
  Serial.begin(230400); 
  pinMode(outputPin, OUTPUT);
  pinMode(zerocross, INPUT_PULLUP);
  Serial.println("welcome");
}

bool attached = false;
void loop() {
  short preVal = acDelayMicroseconds;
  
  if (Serial.available())
  {
    int buf = Serial.parseInt();
    Serial.print("parsed: ");
    Serial.println(buf);
    if (buf <= 0) {
      
    } else if (buf == 1) {
      if (attached) {
        detachInterrupt(digitalPinToInterrupt(zerocross)); 
        attached = false;
      }
      digitalWrite(outputPin, LOW);
    } else if (buf >= 100) {
      if (attached) {
        detachInterrupt(digitalPinToInterrupt(zerocross)); 
        attached = false;
      }
      digitalWrite(outputPin, HIGH);
    } else {
//      6011 - 5101
//      acDelayMi1croseconds = map(buf, 0, 100, 4400, 200);    
      acDelayMicroseconds = map(buf, 0, 100, 7200, 200);    
      if (!attached) {
        Serial.println("Attaching");
        attachInterrupt(digitalPinToInterrupt(zerocross), zero_crosss_int, RISING);
        attached = true;
      }
    }
    delay(200);
  }
  if (preVal != acDelayMicroseconds) {
    Serial.print("new waiting is: ");  
    Serial.println(acDelayMicroseconds);  
  }
  delay(50);

}
